import React from "react";
import MoviesData from "../data/movies.json";
class MoviesItem extends React.Component {
  render() {
    return (
      <div className="movies-container">
        {MoviesData.map((moviesDetail, index) => {
          return (
            <div key={moviesDetail.title} className="movies movies-item">
              <div className="movies-box">
                <div className="movies--item flexible">
                  <div className="movies-left">
                    <h1 className="movies-title">{moviesDetail.title}</h1>
                    <img
                      className="img"
                      src={moviesDetail.posterurl}
                      alt={moviesDetail.title}
                    />
                  </div>
                  <div className="movies-right">
                    <p>
                      <span className="movies-detail">GENRE: </span>
                      {moviesDetail.genres}
                    </p>
                    <p>
                      {" "}
                      <span className="movies-detail">Year: </span>
                      {moviesDetail.year}
                    </p>
                    <p>
                      {" "}
                      <span className="movies-detail">actors: </span>
                      {moviesDetail.actors}
                    </p>
                    <p>
                      {" "}
                      <span className="movies-detail">storyline: </span>
                      {moviesDetail.storyline}
                    </p>
                    <p>
                      {" "}
                      <span className="movies-detail">release Date: </span>
                      {moviesDetail.releaseDate}
                    </p>

                    <p>
                      {" "}
                      <span className="movies-detail">
                        <i className="fas fa-star fa-2x" />
                      </span>
                      {moviesDetail.imdbRating}
                    </p>
                  </div>
                </div>
              </div>
            </div>
          );
        })}
      </div>
    );
  }
}

export default MoviesItem;
