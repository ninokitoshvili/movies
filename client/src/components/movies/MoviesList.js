import React from "react";
import MoviesData from "../data/movies.json";
import { Link } from "@reach/router";
class MoviesList extends React.Component {
  render() {
    return (
      <div className="movies-container">
        {MoviesData.map((moviesDetail, index) => {
          this.aa = () => {
            console.log(moviesDetail);
            return moviesDetail;
          };
          return (
            <div key={moviesDetail.title} className="movies">
              <div className="movies-box">
                <div className="movies--item">
                  <h1 className="movies-title">{moviesDetail.title}</h1>
                  <img
                    className="img"
                    src={moviesDetail.posterurl}
                    alt={moviesDetail.title}
                  />
                  <p>
                    <span className="movies-detail">GENRE: </span>
                    {moviesDetail.genres}
                  </p>
                  <p>
                    {" "}
                    <span className="movies-detail">Year: </span>
                    {moviesDetail.year}
                  </p>
                  <p>
                    {" "}
                    <span className="movies-detail">
                      <i className="fas fa-star fa-2x" />
                    </span>
                    {moviesDetail.imdbRating}
                  </p>

                  <Link to={`/movies/${moviesDetail.title}`}>
                    <button onClick={this.aa} type="button">
                      See More
                    </button>
                  </Link>
                </div>
              </div>
            </div>
          );
        })}
      </div>
    );
  }
}

export default MoviesList;
